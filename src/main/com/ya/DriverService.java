package com.ya;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class DriverService {

    private String driversFolder = "drivers";

    private String geckoDriverDefaultVersion = "v0.18.0";
    private String geckoDriverPattern = "https://github.com/" +
            "mozilla/geckodriver/releases/download/%1$s/geckodriver-%1$s-%2$s.zip";


    private String chromeDriverDefaultVersion = "2.32";
    private String chromeDriverPattern = "https://chromedriver.storage.googleapis.com/%1$s/chromedriver_%2$s.zip";

    private String ieDriverDefaultVersion = "2.50";
    private String ieDriverPattern = "http://selenium-release.storage.googleapis.com/%1$s/IEDriverServer_x64_%1$s.0.zip";

    public void loadDriver() throws IOException {
        File drivers = new File(driversFolder);
        if (!drivers.exists()) {
            drivers.mkdir();
            downloadGeckoDriver();
            downloadIEDriver();
            downloadChromeDriver();
        } else {
            File[] files = drivers.listFiles();
            if (files == null || files.length == 0) {
                downloadGeckoDriver();
                downloadIEDriver();
                downloadChromeDriver();
            }
        }
    }

    private void downloadIEDriver() throws IOException {
        if (isWindows()) {
            String driverUrl = String.format(ieDriverPattern, ieDriverDefaultVersion);
            File zip = new File(driversFolder + File.separator + "IE-x64.zip");
            downloadAndSaveDriver(driverUrl, zip);
        }
    }

    private void downloadChromeDriver() throws IOException {
        String chromeDriverOS = getSystemSubstringChromeDriver();
        String driverUrl = String.format(chromeDriverPattern, chromeDriverDefaultVersion, chromeDriverOS);
        File zip = new File(driversFolder + File.separator + "chrome-" + chromeDriverOS + ".zip");
        downloadAndSaveDriver(driverUrl, zip);
    }

    private String getSystemSubstringGeckoDriver() {
        if (isWindows())
            return "win64";
        if (SystemUtils.IS_OS_LINUX)
            return "linux64";
        return null;
    }

    private boolean isWindows() {
        return SystemUtils.IS_OS_WINDOWS_7
                || SystemUtils.IS_OS_WINDOWS_8 || SystemUtils.IS_OS_WINDOWS_10;
    }

    private String getSystemSubstringChromeDriver() {
        if (isWindows())
            return "win32";
        if (SystemUtils.IS_OS_LINUX)
            return "linux64";
        return null;
    }

    private void downloadGeckoDriver() throws IOException {
        String geckoDriverOS = getSystemSubstringGeckoDriver();
        String driverUrl = String.format(geckoDriverPattern, geckoDriverDefaultVersion, geckoDriverOS);
        File zip = new File(driversFolder + File.separator + "geckodriver-" + geckoDriverOS + ".zip");
        downloadAndSaveDriver(driverUrl, zip);
    }

    private void downloadAndSaveDriver(String win64, File zip) throws IOException {
        FileUtils.copyURLToFile(new URL(win64), zip);
        UnzipUtility.unzip(zip, driversFolder);
        zip.delete();
    }
}
